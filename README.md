# README #
This is my chat app using the MEAN stack, Mongo DB, Express, Angular and Node.
No need to do 'npm install', the modules are already included in this package.

###You will need:###
- [Mongo DB](http://www.mongodb.org/downloads)
- [NodeJS](http://nodejs.org)

### How do I get set up? ###
Fire up a terminal, go to the folder and type:

```
#!node
//this will fire up your mongo daemon
$ sudo mongod 

//this will fire up your mongo daemon
$ npm install 

//This starts the server
$ node server
```

### Screenshot ###
![Screenshot 2015-02-26 14.15.11.PNG](https://bitbucket.org/repo/joaq8y/images/2655432826-Screenshot%202015-02-26%2014.15.11.PNG)