angular.module('myApp', [
    'ngRoute'
])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/',{
            templateUrl: 'pages/home.html',
            controller: 'myController',
            controllerAs: 'app',
            route: '/'
        })
        .when('/home', {
            templateUrl: 'pages/home.html',
            controller: 'myController',
            controllerAs: 'app',
            route: '/home'
        })
        .when('/chat', {
            templateUrl: 'pages/chat.html',
            controller: 'myController',
            controllerAs: 'app',
            route: '/chat'

        })
        .when('/flickr', {
            templateUrl: 'pages/flickr.html',
            controller: 'myController',
            controllerAs: 'app',
            route: '/flickr'

        })
        .when('/about', {
            templateUrl: 'pages/about.html',
            controller: 'myController',
            controllerAs: 'app',
            route: '/about'
        })
        .when('/contact', {
            templateUrl: 'pages/contact.html',
            controller: 'myController',
            controllerAs: 'app',
            route: '/contact'
        })
        .when('/404', {
            templateUrl: 'pages/404.html',
            controller: 'myController',
            controllerAs: 'app',
            route: '/404'
        })
        .otherwise({ redirectTo: '/404' });
}])

.controller('myController', ['$scope', '$http', function($scope, $http){
    var self = this;
    var timeStamp = new Date().toString();
    var apiUrl = "http://localhost:3000/api";
    var text = $scope.flickrSearchQuery;
    var flickerApi = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=005aed3f5711b24c3fc8de9c394b5028&tags='+ text +'&text='+text+'&format=json&api_sig=7b38bbf4386c9505716679af4eeba917';
    $scope.currentURI = window.location.hash;


    self.messageSave = function(e){
        if (e.keyCode !== 13 || $scope.newUser in window || $scope.newMessage in window) {
            console.log('app.js received: '+ $scope.newUser +' '+ $scope.newMessage);
            return false;
        } else {
            var newUserColor = nameToColor($scope.newUser);
            $http.post(apiUrl+'/add', {
                date: timeStamp,
                userName: $scope.newUser,
                userColor: newUserColor,
                message: $scope.newMessage
            })
            .success(function () {
                console.log("app.js posted: "+ $scope.newUser+" - "+$scope.newMessage);
                $scope.newMessage = undefined;

                loadMessages();
                document.getElementById('chat__newmessage').value = '';
                //scrollDown('.chat__messages');
            })
        }
    };

    self.messageUpdate = function(id, newQuery){
        $http.post(apiUrl+"/edit/"+id+"", {name: newQuery}, {"content-type" : "application/json"} ).success(function (message) {
            self.messages =  message;
        });
        console.log("FE updated: " + newQuery);
        loadMessages();
    };

    self.messageDelete = function(id){
        $http.get(apiUrl+"/del/"+id).success(function (message) {
            console.log('sending delete request: ' + id);
            self.messages = message;
        });
        loadMessages();
    };

    /**
     * will return all items
     */
    function loadMessages() {
        $http.get(apiUrl).success(function (messages) {
            self.messages = messages;


            //scroll down every time loadMessages() is called.
            if($('.chat__messages')) {
                $('.chat__messages').animate({scrollTop: $('.chat__messages')[0].scrollHeight + 999999}, 500);
            }
        })
    }

    function nameToColor(name) {
        var n = 'abcdefghijklmnopqrstuvwxyz '.split('');
        var r = name.split('').map(function(e) {return n.indexOf(e);}).join('');
        var l = parseFloat( '0.'+ (r*r*1000).toString().replace(/^0/, ''));
        return '#'+Math.floor(l*16777215).toString(16);
    }

    loadMessages();
}]);