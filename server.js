"use strict";
var mongoose = require('mongoose');
var express = require('express');
var cors = require('cors');
var bodyParser = require("body-parser");

var app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true })); // new method of using body-parser
app.use(bodyParser.json());
app.use(express.static('public'));

/**
 * connect with mongodb and select the "local" database using mongoose;
 */
mongoose.connect('mongodb://localhost/local');

/**
 * Look for the given collection inside mongodb;
 * @type {*|Model}
 */
var timeStamp = new Date();
var myModel = mongoose.model('chatMessages', {date: Date, userName: String, userColor: 'string', message: String});


/**
 * serve up the website
 */


/**
 * GET data from the /api
 */
app.get("/api", function(req, res){
    myModel.find(function(err, messages){
        res.json(messages);
    })
});

/**
 * Save data to the API using POST
 */
app.post("/api/add", function(req, res){
    var newMessage = req.body.message;
    var newUser = req.body.userName;
    var newUserColor = req.body.userColor;
    var message = new myModel({date: timeStamp, userName: newUser, userColor: newUserColor, message: newMessage});
    console.log(newUser, newMessage);
    message.save(function(err){
        if (err) res.sendStatus(500);
        res.send();
    })
});

/**
 * this will update the item with the corresponding id
 */
app.post("/api/edit/:id", function(req, res){
    var itemId = req.params.id;
    var itemQuery = req.body.name;

    myModel.update({_id: itemId}, {name: itemQuery}, function(err){
        if (err){
            console.log("backed failed to update");
            res.sendStatus(500);
        }
        else {
            res.sendStatus(200);
            console.log("updated: "+ itemId + "to: " +itemQuery );
            res.send();
        }
    })
});

/**
 * this will delete the item with the corresponding id
 */
app.get("/api/del/:id", function(req, res){
    var itemId = req.params.id;
    myModel.remove({_id: itemId}, function(err){
        console.log("removed: "+ itemId);
        if (err) res.sendStatus(500);
        res.send();
    })
});

app.listen(3000);